# AOS VISITANTES
Bem vindo(a)!

Esse meu GitLab tem algum tempo de vida, foi criado por março de 2021 quando comecei minha jornada na programação na Kenzie Academy Brasil.

Por isso boa parte dos projetos estão privados, para manter a funcionalidade da Kenzie com novos alunos e outras coisas.

Caso queira conhecer minha trilha de códigos desde o zero até atualmente só mandar uma mensagem no Linkedin (https://www.linkedin.com/in/andluizv/) ou via e-mail (andluizv@msn.com).

Alguns projetos estão publicos, são eles: trabalhos em grupo realizados no final de cada trimestre na Kenzie e alguns desafios pessoais (normalmente ideias tiradas de sites com desafios para praticar).



